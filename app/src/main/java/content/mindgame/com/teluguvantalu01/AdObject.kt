package content.mindgame.com.teluguvantalu01

import android.util.Log
import java.io.IOException
import java.sql.Timestamp
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit
import android.content.Intent
import android.os.Build
import android.support.v4.content.ContextCompat.startActivity
import android.content.ActivityNotFoundException
import android.content.Context
import android.net.Uri
import android.widget.Toast


object AdObject {
    val INTERSTITIAL_TEST_ID = "ca-app-pub-3940256099942544/1033173712" //test ad id
    var PACKAGE_NAME = ""
    val BANNER_TEST_ID = "ca-app-pub-3940256099942544/6300978111"
    var TARGET_DATE_STRING:String =   "10-AUG-2018" //Target Date for the App
    var THRESHOLD_TARGET_HOURS = 12
    val ADS_MODE = "TEST"
    val ADS_MODE_TEST = "TEST"
    val ADS_MODE_PROD = "PROD"
    lateinit var admob:AdmobUtility // to hold admob object
    var APPLICATION_ADS_ID = "" //application ID for ads
    var INTERSTITIAL_ID:String = ""//production interstitial Id
    var BANNER_ID:String ="" //production banner id
    lateinit var LAST_LOADED_SCREEN:()->Unit? /* Last loaded screen function */
    var FRAGMENT_LOADED:Boolean = false

    var TIME_INTERVAL_AD:Int = 60 //Default is 60 sec
    var TIME_LAST_LOADED:Timestamp? = null //last time ad loaded time stamp

//Function to save the Last loaded screen
    fun setLastLoaded(cb:()->Unit){
        LAST_LOADED_SCREEN = cb
        FRAGMENT_LOADED = true
    }

    //Check whether to show the ad or not
    fun showAdOrNot():Boolean{
        var result:Boolean = false
        if (TIME_LAST_LOADED == null) {
            result = true
        } else{
        //Calculate the difference in seconds
        val diff = TimeUnit.MILLISECONDS.toSeconds (Timestamp(Date().time).time - TIME_LAST_LOADED!!.time)
        Log.e("TIME:",diff.toString())
        if (diff >= TIME_INTERVAL_AD){
            result = true
        }     }

        return result

    }

    //Find the Mode of the App based on the date
    fun showAppOrNot():Boolean{
        var result:Boolean = false

        val targetdate:Timestamp =
                Timestamp(SimpleDateFormat("dd-MMM-yyyy", Locale.ENGLISH).parse((TARGET_DATE_STRING)).time)

        val diff_hours = TimeUnit.MILLISECONDS.toHours(targetdate.time - Timestamp(Date().time).time)
        Log.e("Date diff:",targetdate.toString()+":"+diff_hours)

        if (diff_hours<= THRESHOLD_TARGET_HOURS){ //the difference shold be 4 hours or less
            result = true
        }
        return result
    }

    //Check for internet connection
    @Throws(InterruptedException::class, IOException::class)
    fun isConnected(): Boolean {
        val command = "ping -c 1 google.com"
        return Runtime.getRuntime().exec(command).waitFor() == 0
    }

    /*
* Start with rating the app
* Determine if the Play Store is installed on the device
*
* */
    fun rateApp(ctx:Context) {
        try {
            val rateIntent = rateIntentForUrl("market://details")
            startActivity(ctx,rateIntent,null)
        } catch (e: ActivityNotFoundException) {
            Toast.makeText(ctx, "You don't have any app that can open this link", Toast.LENGTH_SHORT).show();
        }
    }




private fun rateIntentForUrl(url: String): Intent {
    val intent = Intent(Intent.ACTION_VIEW, Uri.parse(String.format("%s?id=%s", url, PACKAGE_NAME)))
    var flags = Intent.FLAG_ACTIVITY_NO_HISTORY or Intent.FLAG_ACTIVITY_MULTIPLE_TASK
    if (Build.VERSION.SDK_INT >= 21) {
        flags = flags or Intent.FLAG_ACTIVITY_NEW_DOCUMENT
    } else {

        flags = flags or Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET
    }
    intent.addFlags(flags)
    return intent
}
}