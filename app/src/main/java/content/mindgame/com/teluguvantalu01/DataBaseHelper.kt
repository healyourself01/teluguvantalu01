package content.mindgame.com.teluguvantalu01

import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper


class DataBaseHelper(context: Context, db_name: String) : SQLiteOpenHelper(context, db_name, null, 1) {
    var topicslist = getTopics()

    override fun onCreate(db: SQLiteDatabase) {

    }

    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {

    }

    fun getAllData(tableName: String): Cursor {

        val db = this.writableDatabase

        return db.rawQuery("select * from $tableName", null)

    }

    fun getTopics():Collection<Topic?>{
        var list:ArrayList<Topic?> = ArrayList<Topic?>()

        val qry = "select ID, REMEDIE_NAME from REMEDIE_TYPE"
        val cursor = this.readableDatabase.rawQuery(qry, null)
        if (cursor.moveToFirst()){
            do {
                list.add (
                        Topic(cursor.getInt(0),cursor.getString(1))
                )

            }while (cursor.moveToNext())
        }
        cursor.close()

        return list
    }
    fun getMenus():Collection<Menu?>?{
        var menus:ArrayList<Menu?> = ArrayList<Menu?>()

        val qry = "select TYPE_ID,SUB_TYPE_ID,NAME from REMEDIES where TYPE_ID="+ItemDataset.TOPIC_ID
        val cursor = this.readableDatabase.rawQuery(qry, null)
        if (cursor.moveToFirst()){
            do {
                menus.add (
                        Menu(cursor.getInt(0),cursor.getInt(1),cursor.getString(2))
                )

            }while (cursor.moveToNext())
        }
        cursor.close()

        return menus
    }

    fun getItem():String{
        var itemText:String = "Nothing Found"

        val qry = "select DESCRIPTION from REMEDIES where TYPE_ID="+ItemDataset.TOPIC_ID.toString()+" and " +
                "SUB_TYPE_ID="+ItemDataset.MENU_ID
        val cursor = this.readableDatabase.rawQuery(qry, null)
        if (cursor.moveToFirst()){
             itemText = cursor.getString(0)
        }
        cursor.close()

        return itemText
    }

}

data class Topic(
        var ID:Int,
        var TOPIC_TITLE:String
        )
data class Menu(
        var TOPIC_ID:Int,
        var MENU_ID:Int,
        var MENU_TITLE:String
)
