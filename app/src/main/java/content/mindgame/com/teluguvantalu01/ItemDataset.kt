package content.mindgame.com.teluguvantalu01

import android.util.Log


object ItemDataset {
    var position:Int = 0
    var topics: Collection<Topic>
        get() = kotlin.run {
            try {
                mDbHelper.getTopics()
            } catch (ex:UninitializedPropertyAccessException){
                Log.e("DATABASE","Property not initialized.")
            } } as Collection<Topic>
        set(value) = TODO()

    lateinit var items:Collection<Item>
    var menus: Collection<Menu>
        get() = kotlin.run {
                try {
                    mDbHelper.getMenus()
                } catch (ex:UninitializedPropertyAccessException){
                    Log.e("DATABASE","Property not initialized.")
                } } as Collection<Menu>
        set(value) = TODO()

    var currentMenuTitle: String
        get() = kotlin.run {
                 menus.elementAt(position).MENU_TITLE
                  } as String
        set(value) = TODO()


    var itemText: String
        get() = kotlin.run {
                try {
                    mDbHelper.getItem()
                } catch (ex:UninitializedPropertyAccessException){
                    Log.e("DATABASE","Property not initialized.")
                } } as String
        set(value) = TODO()
//    var itemText = mDbHelper.getItem()
    var TOPIC_ID:Int = 1
    var MENU_ID:Int = 1
    lateinit var mDbHelper: DataBaseHelper

    var ITEM_TEXT =  ""
    lateinit var item_current:Item

    lateinit var admob:AdmobUtility
    val APP_JSON_FILE_NAME = "app_data_full.json"

}