package content.mindgame.com.teluguvantalu01

import android.content.Context
import android.support.v4.app.FragmentActivity
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.menu_item_view.view.*


class TopicAdapter(val ctx:Context, val act:FragmentActivity?):RecyclerView.Adapter<TopicAdapter.ViewHolder>() {


    class ViewHolder(val view:View):RecyclerView.ViewHolder(view){

    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(ctx).inflate(R.layout.menu_item_view,parent,false)
        return ViewHolder(v)
    }

    override fun getItemCount(): Int {
        return ItemDataset.topics.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val title =  ItemDataset.topics.elementAt(position).TOPIC_TITLE //Get the topic title and set it
        holder.view.tvTitle.text = "${position+1} ." + title //Add Index to the title.
        val iconPath = "icons/icon_"+ title.toLowerCase().replace(" ","_")+".png"

//        val img = BitmapFactory.decodeStream(act!!.assets.open(iconPath ))
////        holder.view.imgIcon.setImageResource(icons[position])
//        holder.view.imgIcon.setImageBitmap(img)
//        holder.view.tvLine1.text = ItemDataset.items.elementAt(position).line1

        holder.view.setOnClickListener(View.OnClickListener {
            ItemDataset.TOPIC_ID = ItemDataset.topics.elementAt(position).ID // Save the current topic ID
//            ItemDataset.item_current = ItemDataset.items.elementAt(position)
            if (act is AppInterfaces){
               // act.loadJokeDetail()
                AdObject.admob.loadNextScreen { act.loadJokeMenus() }
            }

        })

    }


}