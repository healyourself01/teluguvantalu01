package content.mindgame.com.teluguvantalu01

import android.content.Context
import android.support.v4.app.FragmentActivity
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.menu_item_view.view.*

class MenuAdapter(val ctx:Context, val act:FragmentActivity?):RecyclerView.Adapter<MenuAdapter.ViewHolder>() {

    class ViewHolder(val v:View):RecyclerView.ViewHolder(v){

    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder{
        val v = LayoutInflater.from(ctx).inflate(R.layout.menu_item_view,parent,false)

        return ViewHolder(v)
    }

    override fun getItemCount(): Int {
         return ItemDataset.menus.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.v.tvTitle.text   = "${position+1}." + ItemDataset.menus.elementAt(position).MENU_TITLE

        holder.v.setOnClickListener(View.OnClickListener {
            ItemDataset.MENU_ID = ItemDataset.menus.elementAt(position).MENU_ID
            ItemDataset.position = position


            if (act is AppInterfaces){
                // act.loadJokeDetail()
                AdObject.admob.loadNextScreen { act.loadJokeDetail() }
            }

        })

    }
}