package content.mindgame.com.teluguvantalu01

interface AppInterfaces {
    fun loadJokeDetail()
    fun loadStartScreen()
    fun loadJokeTopics()
    fun loadJokeMenus()
    fun loadTestModeScreen()
}