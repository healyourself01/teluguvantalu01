package content.mindgame.com.teluguvantalu01

import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.text.Html
import android.text.method.ScrollingMovementMethod
import android.view.*
import android.webkit.WebView
import android.widget.TextView
import android.widget.Toast
import kotlinx.android.synthetic.main.fragment_item.*
import kotlinx.android.synthetic.main.fragment_item.view.*


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [JokeDetailFragment.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [JokeDetailFragment.newInstance] factory method to
 * create an instance of this fragment.
 *
 */
class JokeDetailFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null
    private var itemTitle:MenuItem? =null
    private var wvWebView:WebView? = null
    lateinit var toolbar: android.support.v7.widget.Toolbar
    lateinit var tvBody:TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        // Inflate the layout for this fragment
        val v = inflater.inflate(R.layout.fragment_item, container, false)


/*----------------ACTIONS FOR TOOLBAR-------------------------*/
        //setup pthe action bar
        val act:AppCompatActivity = activity as AppCompatActivity
        v.my_toolbar.title = ItemDataset.currentMenuTitle
        act.setSupportActionBar(v.my_toolbar)

        setHasOptionsMenu(true)
        toolbar = v.my_toolbar
        tvBody = v.tvBody

/*----------------ACTIONS FOR TOOLBAR-------------------------*/



        val position = ItemDataset.position

        /*-------------------------*LOGIC FOR THE WEBVIEW---------------------------------------*/
        v.tvTitle2.text = "${position+1}." +ItemDataset.currentMenuTitle
//        v.wvBody.webViewClient = WebViewClient()
        v.tvTitle2.visibility = View.GONE
//        wvWebView = v.tvBody
//        wvWebView!!.settings.javaScriptEnabled = true
//        wvWebView!!.loadUrl(ItemDataset.item_current.uris[position])
        /*-------------------------*LOGIC FOR THE WEBVIEW---------------------------------------*/

        /*-------------LOGIC TO LOAD THE TEXT FROM SQLITE DB------------------*/

        loadtexttoBody() //Load the text to the Body form sqlite

       //Share button
        v.imgbtnShare.setOnClickListener(View.OnClickListener {
            val sendIntent: Intent = Intent().apply {
                action = Intent.ACTION_SEND
                putExtra(Intent.EXTRA_TEXT, "Test data")
                type = "text/plain"
            }
            startActivity(sendIntent)
        })

        return v

    }


    override fun onCreateOptionsMenu(menu: android.view.Menu?, inflater: MenuInflater?) {
        val mnu = inflater!!.inflate(R.menu.menu,menu)

        setHasOptionsMenu(true)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onPrepareOptionsMenu(menu: android.view.Menu?) {
        itemTitle = menu!!.findItem(R.id.tvItemTitle)
        toolbar.title = "${ItemDataset.position+1}."+ ItemDataset.currentMenuTitle
        super.onPrepareOptionsMenu(menu)

    }


    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
       when(item!!.itemId){
        R.id.next->{
        loadItem(1)


        }

           R.id.prev ->{
            loadItem(-1)
           }
       }
        return super.onOptionsItemSelected(item)
    }

    fun loadItem(index:Int){

       try
        {
            ItemDataset.MENU_ID += index //Increment the Menu Id to get the correct Item
            ItemDataset.position += index // Increment the position to get the correct title

            toolbar.title = "${ItemDataset.position+1}."+ItemDataset.currentMenuTitle
                    AdObject.admob.loadNextScreen {

                       loadtexttoBody()
             }
        }
        catch (ex:ArrayIndexOutOfBoundsException){
            Toast.makeText(activity,"Last Item or First item reached!",Toast.LENGTH_SHORT).show()
        }
    }

    fun loadtexttoBody(){
        tvBody.text = ItemDataset.itemText
        //LOGIC TO LOAD HTML TEXT
//        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
//            tvBody.text = (Html.fromHtml(ItemDataset.itemText,Html.FROM_HTML_MODE_LEGACY))
//
//        } else {
//            tvBody.text = (Html.fromHtml(ItemDataset.itemText))
//        }
        tvBody.movementMethod = ScrollingMovementMethod()
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     *
     *
     * See the Android Training lesson [Communicating with Other Fragments]
     * (http://developer.android.com/training/basics/fragments/communicating.html)
     * for more information.
     */
    }
