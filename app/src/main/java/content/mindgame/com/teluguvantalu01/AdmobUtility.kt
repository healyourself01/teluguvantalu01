package content.mindgame.com.teluguvantalu01

import android.app.ProgressDialog
import android.support.v4.app.FragmentActivity
import android.util.Log
import android.widget.Toast
import com.google.android.gms.ads.AdListener
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.InterstitialAd
import com.google.android.gms.ads.MobileAds
import java.sql.Timestamp
import java.util.*

class AdmobUtility(val ctx:FragmentActivity?, val ADS_MODE:String = AdObject.ADS_MODE_TEST)  {
    val mInterstitialAd = InterstitialAd(ctx)
    var AD_LOADING_CALLED:Boolean = false
    val progress = ProgressDialog(ctx).apply {
        isIndeterminate = true
        setTitle("Loading Ad..")
        setMessage("Please Wait..")
        setCancelable(false)
    }
    var callback: ()->Unit? = {Toast.makeText(ctx,"Ad failed to Load.Callback Not set.No connection.",Toast.LENGTH_SHORT).show()}

    /*********************INITIALIZATION BLOCK******************************/

    init {


        //Set the Adunit ID and the load the add initially
        if (ADS_MODE.equals(AdObject.ADS_MODE_TEST)){
        mInterstitialAd.adUnitId = AdObject.INTERSTITIAL_TEST_ID}
        else{
            //Production Mode and AdIds not blank.
            if (ADS_MODE.equals(AdObject.ADS_MODE_PROD) and AdObject.INTERSTITIAL_ID.isNotEmpty()){
                mInterstitialAd.adUnitId = AdObject.INTERSTITIAL_ID
            }else{
                Toast.makeText(ctx,"Please setup the Interstitial ID.",Toast.LENGTH_SHORT).show()
            }
        }

        if (AdObject.isConnected()){  progress.show() } // Show the progress until the ad is loaded

        loadAdwithConnectivityCheck()
        mInterstitialAd.adListener = object: AdListener() {
            override fun onAdLoaded() {
                // Code to be executed when an ad finishes loading.
               dismissWithExceptionHandling()

            }

            override fun onAdFailedToLoad(errorCode: Int) {
                // Code to be executed when an ad request fails.
                //Load the ad to show next time
                AD_LOADING_CALLED = false
               dismissWithExceptionHandling()
                AdObject.TIME_LAST_LOADED = Timestamp(Date().time)
                loadAdwithConnectivityCheck()

                callback()
            }

            override fun onAdOpened() {
                // Code to be executed when the ad is displayed.
                AD_LOADING_CALLED = false
               dismissWithExceptionHandling()
                AdObject.TIME_LAST_LOADED = Timestamp(Date().time)
                loadAdwithConnectivityCheck()

                callback()

            }

            override fun onAdLeftApplication() {
                // Code to be executed when the user has left the app.
                AD_LOADING_CALLED = false
               dismissWithExceptionHandling()
                AdObject.TIME_LAST_LOADED = Timestamp(Date().time)
                loadAdwithConnectivityCheck()

                callback()
            }

            override fun onAdClosed() {
                // Code to be executed when when the interstitial ad is closed.
                AdObject.TIME_LAST_LOADED = Timestamp(Date().time)
               dismissWithExceptionHandling()
                loadAdwithConnectivityCheck()

                callback()
            }
        }
    }

    fun loadNextScreen( cb:()->Unit){
        callback = cb
        if (!AdObject.showAdOrNot() or !AdObject.isConnected()){ //If the internet not avail or timer<60sec
            callback()
            return
        }
        // Show the Spinner now.
        progress.show()

        if (AD_LOADING_CALLED) {
            if (!showAdwithConnectivityCheck()){callback()} //show the ad if internet is avail only.
        } else {
            loadAdwithConnectivityCheck()
            Log.d("ERROR", "The interstitial wasn't loaded yet. Loading it now and will show it next time.")
           dismissWithExceptionHandling()
           callback() //show the ad next time.
            // if (!showAdwithConnectivityCheck()){callback()} //If the ad is not preloaded, the show is not working.
        }
    }

    fun loadAdwithConnectivityCheck(){
        if (AdObject.isConnected()) {
            mInterstitialAd.loadAd(AdRequest.Builder().build())
           // progress.show()
            AD_LOADING_CALLED = true
        }
    }
    fun showAdwithConnectivityCheck():Boolean{
        if (AdObject.isConnected() and AD_LOADING_CALLED) {
            mInterstitialAd.show()
            return true
        }
        return false
    }

    fun dismissWithExceptionHandling(){
        try {
            progress.dismiss()
        }
        catch (ex:IllegalArgumentException){
            Log.e("ADError","Some error in progress bar.")
        }

    }
}